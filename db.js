const mongoose = require('mongoose');

const {DB_URL} = process.env;

const connect = async () => {
  try {
    await mongoose.connect(DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log('conected to db');
  } catch (err) {
    console.log('error connecting to db', err);
  }
};
module.exports = {DB_URL, connect};
