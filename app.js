require('dotenv').config();
const express = require('express');
const db = require('./db');
const app = express();

const {PORT} = process.env || 4000;

db.connect();

app.use('/', (req, res) => {
  res.json('Hello');
});

app.listen(PORT, () => {
  console.log(`running on http://localhost:${PORT}`);
});
